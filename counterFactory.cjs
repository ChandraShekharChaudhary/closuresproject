
function counterFactory(counter=0){
    function add1(){
        counter+=1;
        return counter;
    }

    return { increment:add1, decrement:function (){
        counter -=1;
        return counter;
    } }
}

module.exports=counterFactory;