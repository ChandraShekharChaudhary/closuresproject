// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned


function limitFunctionCallCount(cb, n) {
    if(typeof cb !== 'function' || typeof n !== 'number'){
        throw new Error('Call back function is not passed in limitFunctionCallCount')
    }
    function invoke(...args){
        n=n-1;
        if(n<0){
            return null;
        }
        return cb.apply(this, args);
    }
    return invoke;
}

module.exports=limitFunctionCallCount;

