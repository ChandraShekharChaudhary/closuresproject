let limitFunctionCallCount=require('../limitFunctionCallCount.cjs');

function cb (a=0 , b=0){
     let sum=a+b;
     return sum;
}

let returnFunction=limitFunctionCallCount(cb , 0);
console.log(returnFunction(2, 3));  
console.log(returnFunction(20,3)); 
console.log(returnFunction()); 
console.log(returnFunction()); 
console.log(returnFunction()); 
console.log(returnFunction()); 
console.log(returnFunction(20)); 

// returnFunction();
// returnFunction();
// returnFunction();
// returnFunction();
// returnFunction();

// returnFunction();
// returnFunction();

