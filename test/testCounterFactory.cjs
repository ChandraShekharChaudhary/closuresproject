
const counterFactory=require('../counterFactory.cjs');
let result = counterFactory();

console.log(result.increment());
console.log(result.decrement());
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());

let result2=counterFactory();
console.log(result2.increment());
console.log(result2.increment());
console.log(result2.increment());
console.log(result2.increment());
console.log(result2.increment());

let result3=counterFactory(5);
console.log(result3.increment());
console.log(result3.increment());
console.log(result3.increment());
console.log(result3.decrement());
console.log(result3.decrement());
console.log(result3.decrement());
console.log(result3.decrement());

