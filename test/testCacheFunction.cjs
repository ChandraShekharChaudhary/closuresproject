let cacheFunction=require('../cacheFunction.cjs');

const cb=(a,b)=>{
    return a+b;
}
let returnFunction=cacheFunction(cb);

let output=returnFunction(2,3);
console.log(output);

let returnFunction2=cacheFunction();
console.log(returnFunction2);
console.log(returnFunction2());
