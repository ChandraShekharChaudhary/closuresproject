// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.


function cacheFunction(cb) {
    if(typeof cb!== 'function'){
        throw new Error('Call back function is not passed in function');
    }
    let cache = {};
    function invoke(...args) {
        let key = JSON.stringify(args);
        if (cache.hasOwnProperty(key)) {
            return cache[key];
        }
        else {
            let result = cb(...args);
            cache[key] = result;
            return result;
        }
    }
    return invoke;
}

module.exports=cacheFunction;
//apply(this, args)